import productDao from "../dao/productDao.js";
const keys = ["id", "name", "author","read"];


const findBook = async (reqID) => {
    const book = await productDao.findBook(reqID);
    return book;
};

const findAll = async () => {
    const books = await productDao.findAll();
    return books.rows;
};

const insertBook = async (book) => {

    let result = 0; // default state : error
    if (keys.every(key => book.hasOwnProperty(key))) {
        result = await productDao.insertBook(book);
    } else {
        console.log(`Please provide all keys ${keys}.`);
    }
    
    return result;
};

const updateBook = async (book,reqID) => {

    let result = 0; // default state : error
    if (keys.every(key => book.hasOwnProperty(key))) {
        result = await productDao.updateBook(book,reqID);
    } else {
        console.log(`Please provide all keys ${keys}.`);
    }
    
    return result;
};

const deleteBook = async (reqID) => {

    const result = await productDao.deleteBook(reqID);
    // console.log(`At service : result is ${result}.`);
    return result;
};

export default { 
    findAll,findBook,insertBook,updateBook,deleteBook
};