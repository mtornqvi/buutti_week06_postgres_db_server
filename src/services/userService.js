import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import userDao from "../dao/userDao.js";

// create passhash from user password with bcrypt
// and store it into database
export const createUser = async (user) => {
    const saltRounds = 10;
    const passhash = await bcrypt.hash(user.password, saltRounds);
    const storableUser = {
        username: user.username,
        passhash
    };
    const result = await userDao.insertUser(storableUser);
    return result;
};

// compare username and password to previously created hash
// if correct, supply token 
// https://jwt.io/
// https://jwt.io/introduction
export const login = async (user) => {
    const userFromDb = await userDao.findUser(user.username);
    const passwordCorrect = userFromDb ? await bcrypt.compare(user.password, userFromDb.passhash) : false;
    if (!user || !passwordCorrect) {
        throw new Error("Wrong username or password");
    }
    const token = jwt.sign(
        { 
            id: user.id,
            username: user.username
        },
        process.env.APP_SECRET, // uses application specific 
        { expiresIn: 60*60 } // expires in one hour
    );
    return token;
};