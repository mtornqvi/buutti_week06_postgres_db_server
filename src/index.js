/**
/api contains all the express endpoints in appropriate routers. Routers should never
bypass services. If you have some dao imported to a router, you are doing it wrong.
- /dao contain data access. These files contain the SQL queries and are responsible
for performing the database operations
- /service contains files containing the business logic. Routers from /api folder use
services. Services use Daos to perform database operations

 */


// Postman : GET http://localhost:3000/books
// list of all books
// Postman : GET http://localhost:3000/books/:id
// Returns a book with a corresponding ID
// Postman : POST http://localhost:3000/books/
// Creates a new book
// Postman : PUT http://localhost:3000/books/:id
// Modifies an existing book
// Postman : DELETE http://localhost:3000/books/:id
// Removes a book with a corresponding id


// Create user name
// Postman : POST http://localhost:3000/user/create
// {"username" : "mikko", "password" : "passu"}
//
// Get user token
// Postman : POST http://localhost:3000/user/login
// {"username" : "mikko", "password" : "passu"}

// Access protected data:
// Postman : GET http://localhost:3000/protected
// Must put token into header in Postman
// Authorization->Bearer token


import express from "express";
import dotenv from "dotenv"; // for passwords
import helmet from "helmet"; // for security
import "express-async-errors";  // enables middleware to handle errors correctly
import logger from "./services/logger.js"; // for logging all queries
import rootRouter from "./api/rootRouter.js";
import productRouter from "./api/productRouter.js";
import protectedRouter from "./api/protectedRouter.js";
import userRouter from "./api/userRouter.js";
import db from "./db/db.js";
import { 
    unknownEndpoint, 
    errorHandler
} from "./middlewares.js";

process.env.NODE_ENV != "prod" && dotenv.config();

const app = express();
app.use (express.json()); // this must be used to parse POST request body made in JSON format (see in Postman)

// enable all helmet security policies, refer to https://github.com/helmetjs/helmet
// details on effects : https://blog.logrocket.com/express-middleware-a-complete-guide/#helmet
// e.g. removed of X-Powered-By from headers
app.use(helmet());

// Start logging requests
app.use(logger);

// Routing
app.use(express.static("public"));
app.use("/", rootRouter);
app.use("/books", productRouter); // everything under /books
app.use("/user", userRouter);
app.use("/protected", protectedRouter);
app.use(unknownEndpoint);
app.use(errorHandler);

process.env.NODE_ENV !== "test" && db.createTables();
const APP_PORT = process.env.APP_PORT || 3000;

(process.env.NODE_ENV !== "test") && app.listen(APP_PORT,  ()  => {
    console.log (`Listening to port ${APP_PORT}`);
});


// must export the Express function => otherwise timeout will occur
export default app;
