# This projects implements

### CRUD functionality 
GET, POST, PUT, DELETE
### PostreSQL database handling in Azure

### JWSON WEb Token implementation (login system)

### Current location :

https://mikko-ci-app.azurewebsites.net/



GET https://mikko-ci-app.azurewebsites.net/books 

GET https://mikko-ci-app.azurewebsites.net/books/:id

POST https://mikko-ci-app.azurewebsites.net/books/

PUT  https://mikko-ci-app.azurewebsites.net/books/:id

DELETE https://mikko-ci-app.azurewebsites.net/books/:id

 Create user name

 Postman : POST https://mikko-ci-app.azurewebsites.net/user/create

 {"username" : "mikko", "password" : "passu"}

 Get user token

 Postman : POST https://mikko-ci-app.azurewebsites.net/user/login

 {"username" : "mikko", "password" : "passu"}

 Access protected data:

 Postman : GET https://mikko-ci-app.azurewebsites.net/protected

 Must put token into header in Postman

 Authorization->Bearer token

## Azure update problems still exist

https://www.how2code.info/en/blog/azure-linux-web-app-not-updating-after-publish/