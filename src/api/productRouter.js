import { Router } from "express";
import productService from "../services/productService.js";

const router = Router();

// everything under /books
router.get("/:id", async (req, res) => {
    const reqID = Number(req.params.id);
    const book = await productService.findBook(reqID);
    // console.log("At router, response was:");
    // console.table(book);
    if (book.rowCount === 0 ) {
        res.status(404).send(`No book found with ID=${reqID}.`);
    }   
    else if (book.rowCount === 1 ) {
        res.status(200).send(book.rows[0]);
    }

});

router.get("/", async (_req, res) => {
    const books = await productService.findAll();
    res.status(200).send(books);
    return;
});

router.post("/", async (req, res) => {
    const book = req.body;
    const result = await productService.insertBook(book);
    res.status(result ? 201 : 500).send(result ? "Created" : "Error");
    return;
});

router.put("/:id", async (req, res) => {
    const book = req.body;
    const reqID = Number(req.params.id);
    const result = await productService.updateBook(book,reqID);

    console.log(`Put /books/${reqID}, result is :`);
    console.table(result);
    console.table(`rowCount = ${result.rowCount}`);

    if (result.rowCount === 0) {
        res.status(404).send(`Didn't find book with ID=${reqID}.`);
    } else if (result.rowCount === undefined) {
        res.status(500).send("Please provide all book parameters.");
    } else {
        res.status(200).send("Book updated succesfully.");
    }
    
});

router.delete("/:id", async (req, res) => {
    const reqID = Number(req.params.id);
    const result = await productService.deleteBook(reqID);
    // console.log(`At router : result is ${result}.`);
    res.status(result ? 200 : 404).send(result ? "Book deleted" : "Error");
    return;
});

export default router;