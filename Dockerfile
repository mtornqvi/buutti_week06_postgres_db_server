# Local build : docker build -t mikko-ci-app --build-arg APP_PORT=3000 .
# Local test : winpty docker run -p 3000:3000 -it mikko-ci-app

FROM node:16 

ARG APP_PG_HOST
ARG APP_PG_USER
ARG APP_PG_DB
ARG APP_PG_PASSWORD
ARG APP_PG_PORT
ARG APP_PORT
ARG APP_SECRET

ENV APP_PG_HOST=${APP_PG_HOST}
ENV APP_PG_USER=${APP_PG_USER}
ENV APP_PG_DB=${APP_PG_DB}
ENV APP_PG_PASSWORD=${APP_PG_PASSWORD}
ENV APP_PG_PORT=${APP_PG_PORT}
ENV APP_PORT=${APP_PORT}
ENV APP_SECRET=${APP_SECRET}

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install 
COPY ./src ./src
# public/index.html => static page
COPY ./public ./public
EXPOSE ${APP_PORT}

CMD ["npm","start"]